from django.db import models


class Artist(models.Model):
    name = models.TextField(unique=True)


class Album(models.Model):
    name = models.TextField(null=False)

    release_date = models.DateField(null=True)
    artists = models.ManyToManyField(Artist, related_name="albums")


class TrackData(models.Model):
    content = models.FileField()


class Track(models.Model):
    name = models.TextField(null=False)
    duration = models.DurationField(null=True)
    format = models.CharField(max_length=5)

    data = models.ForeignKey(TrackData, on_delete=models.CASCADE, null=True)

    album = models.ForeignKey(Album, on_delete=models.SET_NULL, related_name="tracks", null=True)
    artists = models.ManyToManyField(Artist, related_name="tracks")
