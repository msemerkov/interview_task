import datetime

from django.db import transaction, models
import mutagen

from audioservice.models import Track, TrackData, Album


class TrackManager:

    @classmethod
    def from_settings(cls):
        return cls()

    def create_album(self, name, release_date, artists):
        if not name:
            return None

        matching_albums = Album.objects.filter(name=name).annotate(
            artists_count=models.Count("artists")
        ).filter(artists_count=len(artists))
        for artist in artists:
            matching_albums = matching_albums.filter(artists=artist)
            if release_date:
                matching_albums = matching_albums.objects.filter(release_date=release_date)

        if matching_albums:
            album = matching_albums[0]
        else:
            with transaction.atomic():
                album = Album.objects.create(
                    name=name,
                    release_date=release_date,
                )
            album.artists.set(artists)
        return album

    def create_track(self, name, data_file, format, artists=None, duration=None, album=None):
        with transaction.atomic():
            track_data = TrackData.objects.create(
                content=data_file
            )
            track = Track.objects.create(
                name=name,
                format=format,
                duration=datetime.timedelta(seconds=duration),
                data=track_data,
                album=album,
            )
            if artists:
                track.artists.set(artists)
        return track

    def parse_file(self, data_file):
        mfile = mutagen.File(data_file)
        res = {}
        for key, synonyms in [
                ("albums", ("album", "TALB")),
                ("titles", ("title", "TIT2")),
                ("artists", ("artist", "TPE2")),
        ]:
            for s in synonyms:
                if s in mfile:
                    res[key] = mfile[s]
                    break
        res["duration"] = mfile.info.length
        res["format"] = mfile.info.__class__.__module__.split(".")[-1]
        return res
