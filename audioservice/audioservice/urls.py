from django.urls import path

from audioservice.views import TrackListView, TrackDownloadView, TrackUploadView

urlpatterns = [
    path('tracks/', TrackListView.as_view(), name="track_list"),
    path('tracks/upload/', TrackUploadView.as_view(), name="track_upload"),
    path('tracks/<int:track_id>/', TrackDownloadView.as_view(), name="track_download"),
]
