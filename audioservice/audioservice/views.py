import tempfile

from django.shortcuts import get_object_or_404
from django.db import transaction
from rest_framework import generics, views, parsers, response, renderers
from django.http import HttpResponse

from audioservice.models import Track, TrackData, Album, Artist
from audioservice.serializer import TrackSerializer
from audioservice.track_manager import TrackManager


class TrackListView(generics.ListAPIView):
    queryset = Track.objects.all()
    serializer_class = TrackSerializer


class TrackDownloadView(views.APIView):

    def get(self, request, track_id):
        track = get_object_or_404(Track, id=track_id)
        resp = HttpResponse(track.data.content, content_type="application/force-download")
        resp["Content-Disposition"] = f'attachment; filename="{track.name}.{track.format}"'
        resp["Content-type"] = "Content-Type: audio/mpeg"
        return resp


class TrackUploadView(views.APIView):
    parser_classes = [parsers.FileUploadParser]

    def put(self, request):
        data_file = request.data['file']
        track_manager = TrackManager.from_settings()
        parsed = track_manager.parse_file(data_file)

        artist_names = parsed.get("artists", [])
        album_names = parsed.get("albums")
        if album_names:
            album_name = album_names[0]
        track_names = parsed.get("titles")
        if track_names:
            track_name = track_names[0]
        track_duration = parsed["duration"]
        album_release_date = None
        track_format = parsed["format"]

        artists = [
            Artist.objects.get_or_create(name=name)[0]
            for name in artist_names
        ]
        album = track_manager.create_album(album_name, album_release_date, artists)
        track = track_manager.create_track(
            track_name, data_file, track_format, artists=artists,
            duration=track_duration, album=album
        )

        return response.Response(TrackSerializer(track).data)
